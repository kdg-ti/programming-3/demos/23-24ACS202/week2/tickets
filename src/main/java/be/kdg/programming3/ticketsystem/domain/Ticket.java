package be.kdg.programming3.ticketsystem.domain;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class Ticket {
    private static int ticketCounter = 0;

    private final int accountId;
    private final LocalDateTime dateOpened; //use now()!
    private final String text;
    private final int ticketNumber; //?? how to create this?

    private TicketState ticketState; // should be open at the start
    private final List<TicketResponse> ticketResponses; //you should be able to add responses!

    public Ticket(int accountId, String text) {
        this.accountId = accountId;
        this.text = text;
        this.dateOpened = LocalDateTime.now();
        this.ticketNumber = ticketCounter++;//create unique ticketnumber....
        this.ticketState = TicketState.OPEN;
        this.ticketResponses = new ArrayList<>();
    }

    public int getAccountId() {
        return accountId;
    }
    public LocalDateTime getDateOpened() {
        return dateOpened;
    }
    public String getText() {
        return text;
    }
    public int getTicketNumber() {
        return ticketNumber;
    }

    public void addTicketResponse(TicketResponse ticketResponse){
        if (ticketResponse.isClientResponse()) {
            this.ticketState = TicketState.CLIENT_ANSWERED;
        } else {
            this.ticketState = TicketState.ANSWERED;
        }
        ticketResponses.add(ticketResponse);
    }

    public void close(){
        this.ticketState = TicketState.CLOSED;
    }

    @Override
    public String toString() {
        return "Ticket{" +
                "accountId=" + accountId +
                ", dateOpened=" + dateOpened +
                ", text='" + text + '\'' +
                ", ticketNumber=" + ticketNumber +
                ", ticketState=" + ticketState +
                '}';
    }
}
