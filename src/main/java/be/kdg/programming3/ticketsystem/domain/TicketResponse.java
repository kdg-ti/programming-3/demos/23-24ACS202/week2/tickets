package be.kdg.programming3.ticketsystem.domain;

import java.time.LocalDateTime;

public class TicketResponse {
    private static int idCounter = 0;
    private final LocalDateTime date;
    private final int id;
    private final boolean isClientResponse;
    private final String text;

    private final Ticket ticket;

    public TicketResponse(String text, boolean isClientResponse, Ticket ticket) {
        this.isClientResponse = isClientResponse;
        this.text = text;
        this.date = LocalDateTime.now();
        this.id = idCounter++;
        this.ticket = ticket;
    }

    public LocalDateTime getDate() {
        return date;
    }
    public int getId() {
        return id;
    }
    public boolean isClientResponse() {
        return isClientResponse;
    }
    public String getText() {
        return text;
    }
    public Ticket getTicket() {
        return ticket;
    }

    @Override
    public String toString() {
        return "TicketResponse{" +
                "date=" + date +
                ", id=" + id +
                ", isClientResponse=" + isClientResponse +
                ", text='" + text + '\'' +
                ", ticket=" + ticket +
                '}';
    }
}
