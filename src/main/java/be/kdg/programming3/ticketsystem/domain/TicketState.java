package be.kdg.programming3.ticketsystem.domain;

public enum TicketState {
    OPEN, ANSWERED, CLIENT_ANSWERED, CLOSED
}
