package be.kdg.programming3.ticketsystem.domain;

public class HardwareTicket extends Ticket{
    private String deviceName;

    public HardwareTicket(int accountId, String text, String deviceName) {
        super(accountId, text);
        this.deviceName = deviceName;
    }

    @Override
    public String toString() {
        return super.toString() + ", deviceName:" + deviceName;
    }
}
