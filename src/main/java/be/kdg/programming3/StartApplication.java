package be.kdg.programming3;

import be.kdg.programming3.ticketsystem.domain.HardwareTicket;
import be.kdg.programming3.ticketsystem.domain.Ticket;
import be.kdg.programming3.ticketsystem.domain.TicketResponse;

public class StartApplication {
    public static void main(String[] args) {
        Ticket ticket = new Ticket(123, "laptop gives bluescreen");
        TicketResponse ticketResponse = new TicketResponse("Update to latest version",false,ticket);
        ticket.addTicketResponse(ticketResponse);
        Ticket hardwareTicket = new HardwareTicket(123, "laptop not starting","laptop1243");
        TicketResponse ticketResponse2 = new TicketResponse("Plug it in",false,hardwareTicket);
        hardwareTicket.addTicketResponse(ticketResponse2);

        System.out.println(ticket);
        System.out.println(hardwareTicket);
        ticket.close();
        System.out.println(ticket);
    }
}
